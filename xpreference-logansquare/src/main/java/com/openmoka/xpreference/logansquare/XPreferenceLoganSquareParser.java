package com.openmoka.xpreference.logansquare;

import android.support.annotation.NonNull;

import com.bluelinelabs.logansquare.LoganSquare;
import com.openmoka.android.xpreference.XPreferenceException;
import com.openmoka.android.xpreference.XPreferenceModel;
import com.openmoka.android.xpreference.XPreferenceParser;

import java.io.IOException;
import java.util.Locale;

public class XPreferenceLoganSquareParser<TPreference extends XPreferenceModel>
        implements XPreferenceParser<TPreference> {

    public static class Factory implements XPreferenceParser.Factory {

        public static Factory newInstance() {
            return new Factory();
        }

        private Factory() {
        }

        @Override
        public <TPreference extends XPreferenceModel> XPreferenceParser<TPreference>
        getParser(Class<? extends TPreference> clazz) {
            return new XPreferenceLoganSquareParser<>(clazz);
        }
    }

    private final Class<? extends TPreference> clazz;

    private XPreferenceLoganSquareParser(Class<? extends TPreference> clazz) {
        this.clazz = clazz;
    }

    public String serialize(@NonNull TPreference preferenceModel) throws XPreferenceException {
        try {
            return LoganSquare.serialize(preferenceModel);
        } catch (IOException e) {
            final String message = String.format(
                    Locale.US,
                    "A problem occurred while trying to serialize PreferenceModel with id [%s]",
                    preferenceModel.getXPreferenceId()
            );

            throw new XPreferenceException(message, e);
        }
    }

    @Override
    public TPreference parse(@NonNull String json) throws XPreferenceException {
        try {
            return LoganSquare.parse(json, clazz);
        } catch (IOException e) {
            final String message =
                    "A problem occurred while trying to parse preference text";

            throw new XPreferenceException(message, e);
        }
    }
}
