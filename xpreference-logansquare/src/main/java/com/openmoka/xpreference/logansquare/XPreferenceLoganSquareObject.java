package com.openmoka.xpreference.logansquare;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;
import com.openmoka.android.xpreference.XPreferenceObject;

@JsonObject
public class XPreferenceLoganSquareObject extends XPreferenceObject {

    @JsonField
    private String xPreferenceId;

    public XPreferenceLoganSquareObject() {
    }

    @Override
    public String getXPreferenceId() {
        return xPreferenceId;
    }

    @Override
    public void setXPreferenceId(String id) {
        this.xPreferenceId = id;
    }
}
