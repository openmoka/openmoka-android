package com.openmoka.android.widget.fresco;

import android.content.Context;
import android.net.Uri;
import android.util.AttributeSet;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

public class AutoSamplingDraweeView extends SimpleDraweeView {

    public AutoSamplingDraweeView(Context context, GenericDraweeHierarchy hierarchy) {
        super(context, hierarchy);
    }

    public AutoSamplingDraweeView(Context context) {
        super(context);
    }

    public AutoSamplingDraweeView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AutoSamplingDraweeView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public AutoSamplingDraweeView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void setImageURI(final Uri uri, final Object callerContext) {
        if(uri == null){
            super.setImageURI(uri, callerContext);
            return;
        }

        this.post(new Runnable() {
            @Override
            public void run() {
                int width = getMeasuredWidth();
                int height = getMeasuredHeight();

                ImageRequestBuilder requestBuilder = ImageRequestBuilder.newBuilderWithSource(uri);
                if (width > 0 && height > 0) {
                    requestBuilder.setResizeOptions(new ResizeOptions(width, height));
                }

                ImageRequest request = requestBuilder.build();

                DraweeController controller = Fresco.newDraweeControllerBuilder()
                        .setCallerContext(callerContext)
                        .setUri(uri)
                        .setOldController(getController())
                        .setImageRequest(request)
                        .build();

                setController(controller);
            }
        });
    }
}
