/*
 * Copyright (C) 2013 The OpenMoka Project by Mokaware
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.openmoka.android.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/**
 * <p>
 * This View will help in the creation of compound controls by inflating a given layout
 * inside itself and providing a mechanism to performCreate the control without having to
 * worry about the View's lifecycle.
 * </p>
 */
public abstract class BaseCompoundView extends FrameLayout {
    private View mContentView;
    private boolean mInitialized = false;
    private boolean mAddViewAllowed = false;

    public BaseCompoundView(Context context) {
        super(context);
        performCreate(context, null);
    }

    public BaseCompoundView(Context context, AttributeSet attrs) {
        super(context, attrs);
        performCreate(context, attrs);
    }

    public BaseCompoundView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        performCreate(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public BaseCompoundView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        performCreate(context, attrs);
    }

    private void performCreate(Context context, AttributeSet attrs) {
        final String logTag = getLogTag();
        Log.v(logTag, "Starting initialization...");
        if (mInitialized) {
            Log.d(logTag, "The control was already initialized");
            return;
        }

        mAddViewAllowed = true;
        this.onCreate(context, attrs);
        mAddViewAllowed = false;

        if (getContentView() == null) {
            final String msg = "No content view was provided. " +
                    "Make sure you call setContentView during onCreate(...)";
            throw new RuntimeException(msg);
        }

        Log.v(logTag, "Initialized!");
        mInitialized = true;
    }

    protected void setContentView(final View contentView) {
        addView(contentView);
        mContentView = contentView;
    }

    protected void setContentView(@LayoutRes final int layoutId) {
        LayoutInflater inflater = (LayoutInflater)
                getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContentView = inflater.inflate(layoutId, this, true);
    }

    /**
     * Obtains the inflated layout used as content for the current compound view
     * @return The View resulting from inflating the provided layout.
     */
    public final View getContentView() {
        return this.mContentView;
    }

    /**
     * Look for a child view with the given id. If this view has the given id, return this view
     * and tries to cast it to the type of the destination field.
     * @param id The id to search for.
     * @param <TView> Type of the destination field to assign the found View to.
     * @return The view that has the given id in the hierarchy or null
     */
    public final <TView extends View> TView findTypedViewById(int id) {
        return (TView) findViewById(id);
    }

    /**
     * Tries to performCreate the current compound view.
     * @param context Context used to inflate the layout.
     * @param attrs Attributes from XML.
     */
    protected abstract void onCreate(Context context, AttributeSet attrs);

    /**
     * Obtains the log tag for the current instance.
     * @return Log tag to be used for the current instance.
     */
    protected String getLogTag() {
        return getClass().getSimpleName();
    }

    @Override
    public void addView(View child) {
        checkAddViewAllowed();
        super.addView(child);
    }

    @Override
    public void addView(View child, int index) {
        checkAddViewAllowed();
        super.addView(child, index);
    }

    @Override
    public void addView(View child, int width, int height) {
        checkAddViewAllowed();
        super.addView(child, width, height);
    }

    @Override
    public void addView(View child, ViewGroup.LayoutParams params) {
        checkAddViewAllowed();
        super.addView(child, params);
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        checkAddViewAllowed();
        super.addView(child, index, params);
    }

    @Override
    protected boolean addViewInLayout(View child, int index, ViewGroup.LayoutParams params) {
        checkAddViewAllowed();
        return super.addViewInLayout(child, index, params);
    }

    @Override
    protected boolean addViewInLayout(View child, int index, ViewGroup.LayoutParams params, boolean preventRequestLayout) {
        checkAddViewAllowed();
        return super.addViewInLayout(child, index, params, preventRequestLayout);
    }

    private void checkAddViewAllowed() {
        if (!mAddViewAllowed) {
            throw new IllegalStateException(
                    "Calling addView directly isn't supported, " +
                            "please provide a layout with setContentView during onCreate"
            );
        }
    }
}
