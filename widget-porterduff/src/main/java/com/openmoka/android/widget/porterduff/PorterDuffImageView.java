/*
 * Copyright (C) 2013 The OpenMoka Project by Mokaware
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.openmoka.android.widget.porterduff;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * <p>
 * Wrapping layouts inside the PorterDuffImageView will let you provide a drawable
 * to apply to the draw result of the contained layout.
 * </p>
 * <p>
 * See {@link R.styleable#PorterDuffCapable PorterDuffCapable attributes}
 * </p>
 */
public class PorterDuffImageView extends ImageView implements PorterDuffCapable {

    private Paint mSrcPaint;
    private Bitmap mPorterDuffSrcBitmap;
    private Bitmap mResizedSrcBitmap;
    private int mCurrentWidth;
    private int mCurrentHeight;
    private Bitmap mIntermediateDrawBitmap;
    private Canvas mIntermediateDrawCanvas;

    public PorterDuffImageView(Context context) {
        super(context);
        init(context, null);
    }

    public PorterDuffImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public PorterDuffImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public PorterDuffImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        final int width = getMeasuredWidth();
        final int height = getMeasuredHeight();
        if (width == 0 || height == 0 || mPorterDuffSrcBitmap == null) {
            mResizedSrcBitmap = null;
        } else if (width != mCurrentWidth || height != mCurrentHeight) {
            mResizedSrcBitmap = resizeBitmap(mPorterDuffSrcBitmap, width, height);
            mIntermediateDrawBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            mIntermediateDrawCanvas = new Canvas(mIntermediateDrawBitmap);
            mCurrentWidth = width;
            mCurrentHeight = height;
        }
    }

    @Override
    public void draw(Canvas canvas) {
        if (mResizedSrcBitmap == null || mIntermediateDrawBitmap == null) {
            super.draw(canvas);
            return;
        }

        super.draw(mIntermediateDrawCanvas);
        mIntermediateDrawCanvas.drawBitmap(mResizedSrcBitmap, 0, 0, mSrcPaint);
        canvas.drawBitmap(mIntermediateDrawBitmap, 0, 0, null);
    }

    private void init(Context context, AttributeSet attrs) {
        final int mode;
        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.PorterDuffCapable);
            final int srcResId = typedArray.getResourceId(R.styleable.PorterDuffCapable_porterDuffSrc, 0);
            mode = typedArray.getInt(R.styleable.PorterDuffCapable_porterDuffMode, 0);
            setPorterDuffSrcResource(srcResId);
            typedArray.recycle();
        } else {
            mode = 0;
        }

        PorterDuff.Mode porterDuffMode = PorterDuffHelper.getPorterDuffMode(mode);
        setPorterDuffMode(porterDuffMode);

        setWillNotDraw(false);
    }

    /**
     * @inherit
     */
    @Override
    public void setPorterDuffMode(PorterDuff.Mode porterDuffMode) {
        final PorterDuffXfermode xfermode = porterDuffMode != null ?
                new PorterDuffXfermode(porterDuffMode) :
                null;
        mSrcPaint = new Paint();
        mSrcPaint.setXfermode(xfermode);
        mSrcPaint.setAntiAlias(true);
        mSrcPaint.setAlpha(0xFF);
        postInvalidate();
    }

    /**
     * @inherit
     */
    @Override
    public void setPorterDuffSrcResource(@DrawableRes int resId) {
        if (resId == 0) {
            setPorterDuffSrcDrawable(null);
            return;
        }

        Drawable drawable = getResources().getDrawable(resId);
        setPorterDuffSrcDrawable(drawable);
    }

    /**
     * @inherit
     */
    @Override
    public void setPorterDuffSrcDrawable(Drawable drawable) {
        mPorterDuffSrcBitmap = drawableToBitmap(drawable);
        if (mResizedSrcBitmap != null && mCurrentWidth != 0 && mCurrentHeight != 0) {
            mResizedSrcBitmap = resizeBitmap(mPorterDuffSrcBitmap, mCurrentWidth, mCurrentHeight);
        }

        postInvalidate();
    }

    private Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable == null) {
            return null;
        }

        Bitmap bitmap;
        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    private Bitmap resizeBitmap(Bitmap bm, int newWidth, int newHeight) {
        if (bm == null) {
            return null;
        }

        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        return resizedBitmap;
    }
}
