/*
 * Copyright (C) 2013 The OpenMoka Project by Mokaware
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.openmoka.android.widget.porterduff;

import android.graphics.PorterDuff;
import android.support.annotation.Nullable;

/**
 * Created by mdelgado on 22/08/2016.
 */
/* package */ class PorterDuffHelper {
    public static final int MODE_ADD = 1;
    public static final int MODE_CLEAR = 2;
    public static final int MODE_DARKEN = 3;
    public static final int MODE_DST = 4;
    public static final int MODE_DST_ATOP = 5;
    public static final int MODE_DST_IN = 6;
    public static final int MODE_DST_OUT = 7;
    public static final int MODE_DST_OVER = 8;
    public static final int MODE_LIGHTEN = 9;
    public static final int MODE_MULTIPLY = 10;
    public static final int MODE_OVERLAY = 11;
    public static final int MODE_SCREEN = 12;
    public static final int MODE_SRC = 13;
    public static final int MODE_SRC_ATOP = 14;
    public static final int MODE_SRC_IN = 15;
    public static final int MODE_SRC_OUT = 16;
    public static final int MODE_SRC_OVER = 17;
    public static final int MODE_XOR = 18;

    @Nullable
    public static PorterDuff.Mode getPorterDuffMode(int mode) {
        PorterDuff.Mode porterDuffMode;
        switch (mode) {
            case PorterDuffHelper.MODE_ADD:
                porterDuffMode = PorterDuff.Mode.ADD;
                break;
            case PorterDuffHelper.MODE_CLEAR:
                porterDuffMode = PorterDuff.Mode.CLEAR;
                break;
            case PorterDuffHelper.MODE_DARKEN:
                porterDuffMode = PorterDuff.Mode.DARKEN;
                break;
            case PorterDuffHelper.MODE_DST:
                porterDuffMode = PorterDuff.Mode.DST;
                break;
            case PorterDuffHelper.MODE_DST_ATOP:
                porterDuffMode = PorterDuff.Mode.DST_ATOP;
                break;
            case PorterDuffHelper.MODE_DST_IN:
                porterDuffMode = PorterDuff.Mode.DST_IN;
                break;
            case PorterDuffHelper.MODE_DST_OUT:
                porterDuffMode = PorterDuff.Mode.DST_OUT;
                break;
            case PorterDuffHelper.MODE_DST_OVER:
                porterDuffMode = PorterDuff.Mode.DST_OVER;
                break;
            case PorterDuffHelper.MODE_LIGHTEN:
                porterDuffMode = PorterDuff.Mode.LIGHTEN;
                break;
            case PorterDuffHelper.MODE_MULTIPLY:
                porterDuffMode = PorterDuff.Mode.MULTIPLY;
                break;
            case PorterDuffHelper.MODE_OVERLAY:
                porterDuffMode = PorterDuff.Mode.OVERLAY;
                break;
            case PorterDuffHelper.MODE_SCREEN:
                porterDuffMode = PorterDuff.Mode.SCREEN;
                break;
            case PorterDuffHelper.MODE_SRC:
                porterDuffMode = PorterDuff.Mode.SRC;
                break;
            case PorterDuffHelper.MODE_SRC_ATOP:
                porterDuffMode = PorterDuff.Mode.SRC_ATOP;
                break;
            case PorterDuffHelper.MODE_SRC_IN:
                porterDuffMode = PorterDuff.Mode.SRC_IN;
                break;
            case PorterDuffHelper.MODE_SRC_OUT:
                porterDuffMode = PorterDuff.Mode.SRC_OUT;
                break;
            case PorterDuffHelper.MODE_SRC_OVER:
                porterDuffMode = PorterDuff.Mode.SRC_OVER;
                break;
            case PorterDuffHelper.MODE_XOR:
                porterDuffMode = PorterDuff.Mode.XOR;
                break;
            default:
                porterDuffMode = null;
                break;
        }
        return porterDuffMode;
    }
}
