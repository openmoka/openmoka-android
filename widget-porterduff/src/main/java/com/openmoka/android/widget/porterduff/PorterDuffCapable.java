/*
 * Copyright (C) 2013 The OpenMoka Project by Mokaware
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.openmoka.android.widget.porterduff;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;

/**
 * Created by mdelgado on 24/08/2016.
 */
/* package */ interface PorterDuffCapable {

    /**
     * Sets the drawable to be used to apply over the layout
     * @param drawable The drawable to be used
     */
    void setPorterDuffSrcDrawable(Drawable drawable);

    /**
     * Sets the drawable to be used to apply over the layout
     * @param resId The drawable to be used
     */
    void setPorterDuffSrcResource(@DrawableRes int resId);

    /**
     * <p>
     * Sets the {@link android.graphics.PorterDuff.Mode} to draw the given Src.
     * </p>
     * @see PorterDuffFrameLayout#setPorterDuffSrcResource(int)
     * @see PorterDuffFrameLayout#setPorterDuffSrcDrawable(Drawable)
     * @param porterDuffMode The {@link android.graphics.PorterDuff.Mode} used to draw,
     *                       or null to draw without PorterDuff
     */
    void setPorterDuffMode(PorterDuff.Mode porterDuffMode);
}
