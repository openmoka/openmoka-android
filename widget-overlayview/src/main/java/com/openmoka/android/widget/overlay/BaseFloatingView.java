/*
 * Copyright (C) 2013 The OpenMoka Project by Mokaware
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.openmoka.android.widget.overlay;

import android.content.Context;
import android.graphics.PixelFormat;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import java.lang.ref.WeakReference;

/**
 * <p>
 * Use this class to wrap any view and make show it as an overlay window. You can decide the
 * type of window and it's flags to change the overlay behavior.
 * </p>
 * <p>
 * In order to show some overlay windows you might need to include the
 * "android.permission.SYSTEM_ALERT_WINDOW" permission in your AndroidManifest.xml file.
 * In the case that this is needed, for Android M and above you must also check for the overlay
 * permission at runtime, for which there's the
 * {@link com.openmoka.android.util.OverlayUtils#hasOverlayPermission(Context)} method provided,
 * and also the {@link com.openmoka.android.util.OverlayUtils#askOverlayPermission(Context)} method
 * which will launch the system preference activity for the user to grant this permission.
 * </p>
 */
public class BaseFloatingView<TView extends View> {

    private final Context mContext;
    private final boolean keepView;
    private WeakReference<WindowManager> mWindowManagerWeakReference;
    private WeakReference<TView> mViewReference;
    private TView mView;
    private int mWindowFlags;
    private int mWindowType;
    private int mWidth;
    private int mHeight;
    private int mWindowFormat;

    /**
     * <p>
     * Creates and initializes a new OverlayWindow with basic parameters.
     * </p>
     * <p>
     * You'll need to assign a {@link TView View} with {@link #setView(View)}
     * before calling {@link #show()}
     * </p>
     *
     * @param context The {@link Context} that will be used to access system resources and services.
     *                It can be different from the Context used to create the View, and can also
     *                be an {@link android.app.Application Application Context}.
     * @param windowType Window type that will be used to show the view.
     *                   See {@link android.view.WindowManager.LayoutParams} for the allowed
     *                   window types.
     */
    public BaseFloatingView(@NonNull Context context, int windowType) {
        this(context, windowType, 0);
    }

    /**
     * <p>
     * Creates and initializes a new OverlayWindow with basic parameters.
     * </p>
     * <p>
     * You'll need to assign a {@link TView View} with {@link #setView(View)}
     * before calling {@link #show()}
     * </p>
     *
     * @param context The {@link Context} that will be used to access system resources and services.
     *                It can be different from the Context used to create the View, and can also
     *                be an {@link android.app.Application Application Context}.
     * @param windowType Window type that will be used to show the view.
     *                   See {@link android.view.WindowManager.LayoutParams} for the allowed
     *                   window types.
     * @param windowFlags Flags that will be applied to the window.
     *                    See {@link android.view.WindowManager.LayoutParams} for the allowed flags.
     */
    public BaseFloatingView(@NonNull Context context, int windowType, int windowFlags) {
        this(context, windowType, windowFlags, null);
    }

    /**
     * <p>
     * Creates and initializes a new OverlayWindow with given parameters.
     * </p>
     *
     * @param context The {@link Context} that will be used to access system resources and services.
     *                It can be different from the Context used to create the View, and can also
     *                be an {@link android.app.Application Application Context}.
     * @param windowType Window type that will be used to show the view.
     *                   See {@link android.view.WindowManager.LayoutParams} for the allowed
     *                   window types.
     * @param windowFlags Flags that will be applied to the window.
     *                    See {@link android.view.WindowManager.LayoutParams} for the allowed flags.
     * @param view {@link View} to be shown as content for the overlay window.
     */
    public BaseFloatingView(@NonNull Context context, int windowType, int windowFlags, TView view) {
        this.mContext = context;
        this.mWindowFlags = windowFlags;
        this.mWindowType = windowType;
        this.mView = view;
        this.keepView = view != null;
        this.mViewReference = new WeakReference<>(view);
    }

    /**
     * Returns the Context this BaseFloatingView is using to access the system resources.
     * @return The BaseFloatingView's Context.
     */
    public Context getContext() {
        return mContext;
    }

    /**
     * Returns the View that's being used as content for this BaseFloatingView.
     * Use this method ONLY while {@link #isShowing()} is true, otherwise there's no certainty it'll
     * return an object or null.
     * @return The current View
     */
    public TView getView() {
        return mViewReference.get();
    }

    /**
     * Replaces the current View for the given one to be used as content.
     * @param view The new View to be used.
     */
    public void setView(@NonNull TView view) {
        this.mView = view;
    }

    /**
     * Returns the current flags that are being used to open the Window. If either
     * {@link #setWindowFlags(int)}, {@link #addWindowFlags(int)} or {@link #removeWindowFlags(int)}
     * was called after calling {@link #show()}, this flags will not be the same ones applied
     * to the Window, unless you have called {@link #updateViewLayout()}.
     * @return The current flags to be used to display the Window.
     */
    public int getWindowFlags() {
        return mWindowFlags;
    }

    /**
     * <p>
     * Replaces the current flags with the given ones. If the window is already showing, this
     * flags will not be applied until you call {@link #updateViewLayout()}, or {@link #hide()}
     * and {@link #show()}.
     * </p>
     * <p>
     * See {@link android.view.WindowManager.LayoutParams} for the allowed
     * window flags.
     * </p>
     * @param windowFlags The new flags to be applied to the window.
     */
    public void setWindowFlags(int windowFlags) {
        this.mWindowFlags = windowFlags;
    }

    /**
     * <p>
     * Adds the given flags to the existing ones. If the window is already showing, this
     * flags will not be applied until you call {@link #updateViewLayout()}, or {@link #hide()}
     * and {@link #show()}.
     * </p>
     * <p>
     * See {@link android.view.WindowManager.LayoutParams} for the allowed
     * window flags.
     * </p>
     * @param windowFlags The new flags to be applied to the window.
     */
    public void addWindowFlags(int windowFlags) {
        this.mWindowFlags |= windowFlags;
    }

    /**
     * <p>
     * Removes the given flags from the existing ones. If the window is already showing, this
     * flags will not be applied until you call {@link #updateViewLayout()}, or {@link #hide()}
     * and {@link #show()}.
     * </p>
     * <p>
     * See {@link android.view.WindowManager.LayoutParams} for the allowed
     * window flags.
     * </p>
     * @param windowFlags The new flags to be applied to the window.
     */
    public void removeWindowFlags(int windowFlags) {
        this.mWindowFlags &= (~windowFlags);
    }

    /**
     * Returns the current window type. This might be different from the actual type of the window
     * showing if you called {@link #setWindowType(int)} after {@link #show()}.
     * @return The current window type.
     */
    public int getWindowType() {
        return mWindowType;
    }

    /**
     * <p>
     * Sets a new window type to be used. If you're calling this method when the window is already
     * showing, you'll need to call {@link #updateViewLayout()} in order for it to be applied.
     * </p>
     * <p>
     * See {@link android.view.WindowManager.LayoutParams} for the allowed
     * window types.
     * </p>
     * @param windowType The new window type to be used.
     */
    public void setWindowType(int windowType) {
        this.mWindowType = windowType;
    }

    /**
     * Returns the current window format. This might be different from the actual format of the
     * window showing if you called {@link #setWindowFormat(int)} after {@link #show()}
     * @return The current window type.
     */
    public int getWindowFormat() {
        return mWindowFormat;
    }

    /**
     * <p>
     * Sets a new window format to be used. If you're calling this method when the window is already
     * showing, you'll need to call {@link #updateViewLayout()} in order for it to be applied.
     * </p>
     * <p>
     * See {@link PixelFormat} for the allowed formats.
     * </p>
     * @param windowFormat The new window type to be used.
     */
    public void setWindowFormat(int windowFormat) {
        this.mWindowFormat = windowFormat;
    }

    /**
     * Returns the current window width. This might be different from the actual type of the window
     * showing if you called {@link #setWidth(int)} after {@link #show()}.
     * @return
     */
    public int getWidth() {
        return mWidth;
    }

    /**
     * Sets a new window width to be used. If you're calling this method when the window is already
     * showing, you'll need to call {@link #updateViewLayout()} in order for it to be applied.
     * @param width The new width to be used.
     */
    public void setWidth(int width) {
        this.mWidth = width;
    }

    /**
     * Returns the current window height. This might be different from the actual type of the window
     * showing if you called {@link #setHeight(int)} after {@link #show()}.
     * @return
     */
    public int getHeight() {
        return mHeight;
    }

    /**
     * Sets a new window height to be used. If you're calling this method when the window is already
     * showing, you'll need to call {@link #updateViewLayout()} in order for it to be applied.
     * @param height The new height to be used
     */
    public void setHeight(int height) {
        this.mHeight = height;
    }

    /**
     * Creates a new window to display the given View, and shows it according to the given parameters.
     * It does nothing if the view is already being shown.
     */
    public void show() {
        if (isShowing()) {
            return;
        }

        // preserve the view object while showing
        mView = getView();
        if (mView == null) {
            mView = onCreateView();
            mViewReference = new WeakReference<>(mView);
        }

        if (mView == null) {
            final String msg = "Error trying to show floating view, no view was provided. Either provide a view through the constructor or overriding onCreateView";
            Log.e(getLogTag(), msg);

            return;
        }

        final WindowManager.LayoutParams layoutParams = createLayoutParams();
        getWindowManager().addView(mView, layoutParams);
    }

    /**
     * Hides the window that's showing the current View. It does nothing if the view isn't
     * being shown.
     */
    public void hide() {
        if (!isShowing()) {
            return;
        }

        getWindowManager().removeView(getView());
        if (!keepView) {
            mView = null;
        }
    }

    /**
     * Returns whether the current View is being shown or not.
     * @return <b>true</b> if the View is being shown, <b>false</b> otherwise.
     */
    public boolean isShowing() {
        return getView() != null && getView().getParent() != null;
    }

    /**
     * Applies the current parameters to the window that's showing the current View. It does nothing
     * if the View isn't being shown.
     */
    public void updateViewLayout() {
        if (!isShowing()) {
            return;
        }

        final WindowManager.LayoutParams layoutParams = createLayoutParams();
        getWindowManager().updateViewLayout(getView(), layoutParams);
    }

    /**
     * Creates and returns the {@link android.view.WindowManager.LayoutParams} that will be used
     * when showing and updating the window.
     * @return The new LayoutParams to be applied to the window.
     */
    @NonNull
    protected WindowManager.LayoutParams createLayoutParams() {
        final int format = getWindowFormat();
        return new WindowManager.LayoutParams(
                mWidth,
                mHeight,
                mWindowType,
                mWindowFlags,
                format == 0 ? PixelFormat.TRANSLUCENT : format
        );
    }

    /**
     * Gets the current WindowManager, or asks the system for it it it's already been collected.
     * @return The current WindowManager, or a new one.
     */
    protected final WindowManager getWindowManager() {
        WindowManager windowManager = extractWindowManagerFromWeakReference();
        if (windowManager == null) {
            windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            mWindowManagerWeakReference = new WeakReference<>(windowManager);
        }

        return windowManager;
    }

    /**
     * Creates the content view whenever needed, if you use the constructor which doesn't receive
     * a view, or provide a null view at the beginning, this method will be called to create new
     * views when needed.
     *
     * The super method returns null, so there's no need to call it when you override it.
     *
     * @return
     */
    protected TView onCreateView() {
        return null;
    }

    protected final String getLogTag() {
        return getClass().getSimpleName();
    }

    private WindowManager extractWindowManagerFromWeakReference() {
        if (mWindowManagerWeakReference == null) {
            return null;
        }

        return mWindowManagerWeakReference.get();
    }
}
