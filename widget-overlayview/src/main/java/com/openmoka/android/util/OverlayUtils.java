package com.openmoka.android.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;

/**
 * Utility class which helps with common operations related to overlay views
 */
public class OverlayUtils {
    /**
     * Returns whether the app can draw overlay windows. This is necessary just
     * for some types of windows.
     * @return <b>true</b> if this app has been granted permission to draw overlay windows.
     */
    @RequiresApi(Build.VERSION_CODES.M)
    public static boolean hasOverlayPermission(@NonNull Context context) {
        return Settings.canDrawOverlays(context);
    }

    /**
     * Starts the settings activity where the user needs to authorize the app
     * to draw overlay windows.
     */
    @RequiresApi(Build.VERSION_CODES.M)
    public static void askOverlayPermission(@NonNull Context context) {
        final String packageName = context.getApplicationInfo().packageName;
        final Uri uri = Uri.parse(String.format("package:%s", packageName));
        try {
            final Intent intent = new Intent()
                    .setAction(Settings.ACTION_MANAGE_OVERLAY_PERMISSION)
                    .setData(uri)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(intent);
            return;
        } catch (Exception ignored) {
        }

        try {
            final Intent intent = new Intent()
                    .setAction(Settings.ACTION_MANAGE_OVERLAY_PERMISSION)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(intent);
            return;
        } catch (Exception ignored) {
        }
    }
}
