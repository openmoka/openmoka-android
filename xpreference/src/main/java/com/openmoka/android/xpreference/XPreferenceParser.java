package com.openmoka.android.xpreference;

import android.support.annotation.NonNull;

public interface XPreferenceParser<TPreference extends XPreferenceModel> {

    interface Factory {
        <TPreference extends XPreferenceModel>
        XPreferenceParser<TPreference> getParser(Class<? extends TPreference> clazz);
    }

    String serialize(@NonNull TPreference preferenceModel) throws XPreferenceException;
    TPreference parse(@NonNull String json) throws XPreferenceException;
}
