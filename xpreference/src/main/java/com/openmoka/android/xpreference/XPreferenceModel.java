package com.openmoka.android.xpreference;

/**
 * Objects which want to be saved through XPreference must implement this interface.
 */
public interface XPreferenceModel {

    /**
     * Gets the current id, which is also used as Preference key
     * @return the current id
     */
    String getXPreferenceId();

    /**
     * Sets the current id, be careful to set this before saving the object, as changing the id
     * will make the next save to use the new id as the Preference key.
     * @param id
     */
    void setXPreferenceId(String id);

    /**
     * Serializes the current object and saves it to SharedPreferences through XPreference
     */
    void save() throws XPreferenceException;
}
