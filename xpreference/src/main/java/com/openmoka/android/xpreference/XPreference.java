package com.openmoka.android.xpreference;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;

public class XPreference {
    private static final String TAG = XPreference.class.getSimpleName();
    private static final String MESSAGE_INSTANCE_INITIALIZING = "Initializing instance";
    private static final String MESSAGE_INSTANCE_INITIALIZED = "Instance initialized";
    private static final String ERROR_MESSAGE_INSTANCE_ALREADY_INITIALIZED =
            "Instance was already initialized. Current initialization call will be ignored";
    protected static final String WARNING_MESSAGE_CONTEXT_NOT_APPLICATION =
            "Provided context wasn't an Application context. The Application context will be instead.";

    public static final String SHARED_PREFERENCES_NAME = "xpreference";

    private static SoftReference<Context> contextSoftReference;
    private static WeakReference<SharedPreferences> sharedPreferencesWeakReference;
    private static String sharedPreferencesName;
    private static XPreferenceParser.Factory parserFactory;

    public static void initialize(@NonNull Context context,
                                  @NonNull XPreferenceParser.Factory parserFactory) {
        initialize(context, parserFactory, SHARED_PREFERENCES_NAME);
    }

    public static void initialize(@NonNull Context context,
                                  @NonNull XPreferenceParser.Factory parserFactory,
                                  @NonNull String sharedPreferenceName) {
        Log.d(TAG, MESSAGE_INSTANCE_INITIALIZING);
        if (getContext() != null) {
            Log.w(TAG, ERROR_MESSAGE_INSTANCE_ALREADY_INITIALIZED);
            return;
        }

        if (!(context instanceof Application)) {
            Log.w(TAG, WARNING_MESSAGE_CONTEXT_NOT_APPLICATION);
        }

        XPreference.sharedPreferencesName = sharedPreferenceName;
        XPreference.contextSoftReference = new SoftReference<>(context.getApplicationContext());
        XPreference.parserFactory = parserFactory;
        Log.d(TAG, MESSAGE_INSTANCE_INITIALIZED);
    }

    public static void savePreference(@NonNull XPreferenceModel preferenceModel)
            throws XPreferenceException {
        final Context context = getContext();
        if (context == null) {
            return;
        }

        final String id = preferenceModel.getXPreferenceId();
        final XPreferenceParser<XPreferenceModel> parser =
                parserFactory.getParser(preferenceModel.getClass());

        final String value = parser.serialize(preferenceModel);

        final SharedPreferences preferences = getSharedPreferences(context);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putString(id, value);
        editor.apply();
    }

    public static <T extends XPreferenceModel> T loadPreference(@NonNull Class<T> clazz,
                                                                @NonNull String id)
            throws XPreferenceException {
        final Context context = getContext();
        final String value;
        if (context != null) {
            final SharedPreferences preferences = getSharedPreferences(context);
            if (preferences != null) {
                value = preferences.getString(id, null);
            } else {
                value = null;
            }
        } else {
            value = null;
        }

        T object = null;
        if (!TextUtils.isEmpty(value)) {
            XPreferenceParser<T> parser = parserFactory.getParser(clazz);
            object = parser.parse(value);
        }

        if (object == null) {
            try {
                final Constructor<T> constructor = clazz.getDeclaredConstructor();
                constructor.setAccessible(true);
                object = constructor.newInstance();
            } catch (Exception e) {
                throw new XPreferenceException("The class must provide a default constructor", e);
            }
        }

        object.setXPreferenceId(id);
        return object;
    }

    private static Context getContext() {
        return contextSoftReference != null ? contextSoftReference.get() : null;
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        SharedPreferences preferences = sharedPreferencesWeakReference != null ?
                sharedPreferencesWeakReference.get() : null;

        if (preferences == null) {
            preferences = context.getSharedPreferences(
                    sharedPreferencesName,
                    Context.MODE_PRIVATE
            );

            sharedPreferencesWeakReference = new WeakReference<>(preferences);
        }

        return preferences;
    }
}
