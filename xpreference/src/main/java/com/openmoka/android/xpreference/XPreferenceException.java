package com.openmoka.android.xpreference;

public class XPreferenceException extends Exception {

    public XPreferenceException(String message) {
        super(message);
    }

    public XPreferenceException(Throwable cause) {
        super(cause);
    }

    public XPreferenceException(String message, Throwable e) {
        super(message, e);
    }
}
