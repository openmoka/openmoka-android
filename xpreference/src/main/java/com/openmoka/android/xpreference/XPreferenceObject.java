package com.openmoka.android.xpreference;

import android.text.TextUtils;

import java.util.UUID;


public class XPreferenceObject implements XPreferenceModel {

    private String xPreferenceId;

    protected XPreferenceObject() {
        this(null);
    }

    protected XPreferenceObject(String xPreferenceId) {
        if (TextUtils.isEmpty(xPreferenceId)) {
            xPreferenceId = UUID.randomUUID().toString();
        }

        setXPreferenceId(xPreferenceId);
    }

    @Override
    public String getXPreferenceId() {
        return this.xPreferenceId;
    }

    @Override
    public void save() throws XPreferenceException {
        XPreference.savePreference(this);
    }

    @Override
    public void setXPreferenceId(String id) {
        this.xPreferenceId = id;
    }
}
