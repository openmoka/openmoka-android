/*
 * Copyright (C) 2013 The OpenMoka Project by Mokaware
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.openmoka.android.app;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.openmoka.android.widget.porterduff.PorterDuffFrameLayout;

public class PorterDuffFrameLayoutTestActivity extends AppCompatActivity {

    private static final PorterDuff.Mode[] MODES = {
            null,
            PorterDuff.Mode.ADD,
            PorterDuff.Mode.CLEAR,
            PorterDuff.Mode.DARKEN,
            PorterDuff.Mode.DST,
            PorterDuff.Mode.DST_ATOP,
            PorterDuff.Mode.DST_IN,
            PorterDuff.Mode.DST_OUT,
            PorterDuff.Mode.DST_OVER,
            PorterDuff.Mode.LIGHTEN,
            PorterDuff.Mode.MULTIPLY,
            PorterDuff.Mode.OVERLAY,
            PorterDuff.Mode.SCREEN,
            PorterDuff.Mode.SRC,
            PorterDuff.Mode.SRC_ATOP,
            PorterDuff.Mode.SRC_IN,
            PorterDuff.Mode.SRC_OUT,
            PorterDuff.Mode.SRC_OVER,
            PorterDuff.Mode.XOR
    };

    private static final int[] SOURCES = {
            android.R.drawable.sym_action_email,
            R.drawable.src_cut_oval,
            R.drawable.src_gradient
    };

    private Spinner mSpinner;
    private RadioGroup mRadioGroup;
    private PorterDuffFrameLayout mPorterDuffFrameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_porter_duff_frame_layout_test);
        mSpinner = (Spinner) findViewById(R.id.porterDuffModeSpinner);
        mRadioGroup = (RadioGroup) findViewById(R.id.porterDuffSrcRadioGroup);
        mPorterDuffFrameLayout = (PorterDuffFrameLayout) findViewById(R.id.porterDuffLayout);

        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                updatePorterDuffLayout();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            updatePorterDuffLayout();
            }
        });
        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                updatePorterDuffLayout();
            }
        });
        updatePorterDuffLayout();
    }

    private void updatePorterDuffLayout() {
        int selectedMode = mSpinner.getSelectedItemPosition();
        int selectedSrc = mRadioGroup.indexOfChild(
                mRadioGroup.findViewById(mRadioGroup.getCheckedRadioButtonId())
        );
        PorterDuff.Mode mode = MODES[selectedMode];
        int srcRes = SOURCES[selectedSrc];
        mPorterDuffFrameLayout.setPorterDuffMode(mode);
        mPorterDuffFrameLayout.setPorterDuffSrcResource(srcRes);
    }
}
