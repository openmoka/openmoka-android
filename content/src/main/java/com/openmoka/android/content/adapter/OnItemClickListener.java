package com.openmoka.android.content.adapter;

public interface OnItemClickListener<T> {
    void onItemClick(int position, T item);
}