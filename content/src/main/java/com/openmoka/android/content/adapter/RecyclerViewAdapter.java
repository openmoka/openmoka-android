package com.openmoka.android.content.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public abstract class RecyclerViewAdapter
        <TData, TViewHolder extends RecyclerViewAdapter.ViewHolder<TData>>
        extends RecyclerView.Adapter<TViewHolder> {

    public static abstract class ViewHolder<TData> extends RecyclerView.ViewHolder {
        private TData data;

        public ViewHolder(ViewGroup parent, @LayoutRes int layoutId) {
            super(LayoutInflater.from(parent.getContext()).inflate(
                    layoutId,
                    parent,
                    false
            ));
        }

        protected void setOnClickListener(final OnItemClickListener<TData> listener) {
            if (listener == null) {
                this.itemView.setOnClickListener(null);
                return;
            }

            this.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(getAdapterPosition(), getData());
                }
            });
        }

        protected void setOnLongClickListener(final OnItemLongClickListener<TData> listener) {
            if (listener == null) {
                this.itemView.setOnLongClickListener(null);
                return;
            }

            this.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    return listener.onItemLongClick(getAdapterPosition(), getData());
                }
            });
        }

        protected void setData(TData data) {
            this.data = data;
            onBindData(data);
        }

        protected TData getData() {
            return data;
        }

        protected abstract void onBindData(TData data);
    }

    private final List<TData> dataList;
    private OnItemClickListener<TData> onItemClickListener;
    private OnItemLongClickListener<TData> onItemLongClickListener;

    public RecyclerViewAdapter() {
        dataList = new ArrayList<>();
    }

    public RecyclerViewAdapter(List<? extends TData> dataList) {
        this();
        if (dataList != null) {
            this.setDataList(dataList);
        }
    }

    @Override
    public void onBindViewHolder(TViewHolder holder, int position) {
        holder.setData(getData(position));
        holder.setOnClickListener(onItemClickListener);
        holder.setOnLongClickListener(onItemLongClickListener);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void setDataList(@NonNull List<? extends TData> dataList) {
        this.dataList.clear();
        this.dataList.addAll(dataList);
    }

    public void setOnItemClickListener(OnItemClickListener<TData> onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener<TData> onItemLongClickListener) {
        this.onItemLongClickListener = onItemLongClickListener;
    }

    public TData getData(int position) {
        return dataList.get(position);
    }
}
