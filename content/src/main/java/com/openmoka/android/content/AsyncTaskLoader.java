package com.openmoka.android.content;

import android.content.Context;
import android.os.Bundle;

public abstract class AsyncTaskLoader<T> extends android.support.v4.content.AsyncTaskLoader<LoaderCallbacks.Result<T>> {

    private final Bundle args;

    public AsyncTaskLoader(Context context, Bundle args) {
        super(context);
        this.args = args;
    }

    @Override
    public final LoaderCallbacks.Result<T> loadInBackground() {
        try {
            final T data = doLoadInBackground(getContext(), args);
            return new LoaderCallbacks.Result<>(data);
        } catch (Exception e) {
            return new LoaderCallbacks.Result<>(e);
        }
    }

    protected abstract T doLoadInBackground(Context context, Bundle args) throws Exception;
}
