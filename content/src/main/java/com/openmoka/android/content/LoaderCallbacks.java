package com.openmoka.android.content;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import java.lang.ref.WeakReference;

public abstract class LoaderCallbacks<T>
        implements LoaderManager.LoaderCallbacks<LoaderCallbacks.Result<T>> {

    public interface Listener<TData> {
        void onLoadFinished(TData data);
        void onError(Exception exception);
    }

    static class Result<T> {
        final T data;
        final boolean isError;
        final Exception exception;

        public Result(T data) {
            this(
                    data,
                    false,
                    null
            );
        }

        public Result(Exception exception) {
            this(
                    null,
                    true,
                    exception
            );
        }

        private Result(T data, boolean isError, Exception exception) {
            this.data = data;
            this.isError = isError;
            this.exception = exception;
        }
    }

    private WeakReference<Listener<T>> listenerWeakReference;
    private final Handler handler;
    private final Context context;

    public LoaderCallbacks(Context context, Listener<T> listener) {
        this.context = context;
        handler = new Handler(Looper.getMainLooper());
        listenerWeakReference = new WeakReference<>(listener);
    }

    public Context getContext() {
        return context;
    }

    @Override
    public final Loader<Result<T>> onCreateLoader(int id, Bundle args) {
        return doCreateLoader(id, args);
    }

    @Override
    public void onLoadFinished(Loader<Result<T>> loader, Result<T> result) {
        if (result.isError) {
            dispatchError(result.exception);
        } else {
            dispatchData(result.data);
        }
    }

    @Override
    public void onLoaderReset(Loader<Result<T>> loader) {

    }

    protected abstract AsyncTaskLoader<T> doCreateLoader(int id, Bundle args);

    private void dispatchError(final Exception exception) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                final Listener<T> listener = listenerWeakReference.get();
                if (listener != null) {
                    listener.onError(exception);
                }
            }
        });
    }

    private void dispatchData(final T data) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                final Listener<T> listener = listenerWeakReference.get();
                if (listener != null) {
                    listener.onLoadFinished(data);
                }
            }
        });
    }
}
