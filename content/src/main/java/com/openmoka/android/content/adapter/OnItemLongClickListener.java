package com.openmoka.android.content.adapter;

public interface OnItemLongClickListener<T> {
    boolean onItemLongClick(int position, T item);
}