package com.openmoka.android.content.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v7.app.AppCompatActivity;

import com.openmoka.android.content.DataLoader;
import com.openmoka.android.content.LoaderCallbacks;

import java.util.List;

public abstract class AsyncAdapterLoader
        <TData, TViewHolder extends RecyclerViewAdapter.ViewHolder<TData>>
        extends RecyclerViewAdapter<TData, TViewHolder> {

    private final LoaderCallbacks.Listener<List<TData>> listener;

    private final DataLoader.Callbacks<List<TData>> dataLoaderCallbacks =
            new DataLoader.Callbacks<List<TData>>() {
                @Override
                public void onLoadFinished(List<TData> dataList) {
                    setDataList(dataList);
                    if (listener != null) {
                        listener.onLoadFinished(dataList);
                    }

                    notifyDataSetChanged();
                }

                @Override
                public void onError(Exception e) {
                    if (listener != null) {
                        listener.onError(e);
                    }
                }

                @Override
                public List<TData> onLoadInBackground(Context context, Bundle args) throws Exception {
                    return AsyncAdapterLoader.this.onLoadInBackground(context, args);
                }
            };

    private final DataLoader<List<TData>> dataLoader;

    public AsyncAdapterLoader() {
        this(null, null);
    }

    public AsyncAdapterLoader(List<? extends TData> dataList) {
        this(dataList, null);
    }

    public AsyncAdapterLoader(LoaderCallbacks.Listener<List<TData>> listener) {
        this(null, listener);
    }

    public AsyncAdapterLoader(List<? extends TData> dataList,
                              LoaderCallbacks.Listener<List<TData>> listener) {
        super(dataList);
        this.listener = listener;
        this.dataLoader = new DataLoader<>(dataLoaderCallbacks);
    }

    public void startLoading(Fragment fragment) {
        dataLoader.startLoading(fragment);
    }

    public void startLoading(Fragment fragment, Bundle args) {
        dataLoader.startLoading(fragment, args);
    }

    public void startLoading(AppCompatActivity activity) {
        dataLoader.startLoading(activity);
    }

    public void startLoading(AppCompatActivity activity, Bundle args) {
        dataLoader.startLoading(activity, args);
    }

    public void startLoading(Context context, LoaderManager loaderManager) {
        dataLoader.startLoading(context, loaderManager);
    }

    public void startLoading(Context context, LoaderManager loaderManager, Bundle args) {
        dataLoader.startLoading(context, loaderManager, args);
    }

    protected abstract List<TData> onLoadInBackground(Context context, Bundle args) throws Exception;
}
