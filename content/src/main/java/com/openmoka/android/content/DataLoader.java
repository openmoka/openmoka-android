package com.openmoka.android.content;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v7.app.AppCompatActivity;

public class DataLoader<TOutputData> {

    private static class InnerCallbacks<TData> extends LoaderCallbacks<TData> {
        private final OnLoadInBackgroundListener<TData> loadInBackgroundListener;

        private InnerCallbacks(Context context,
                               Listener<TData> listener,
                               OnLoadInBackgroundListener<TData> loadInBackgroundListener) {
            super(context, listener);
            this.loadInBackgroundListener = loadInBackgroundListener;
        }

        @Override
        protected AsyncTaskLoader<TData> doCreateLoader(int i, Bundle args) {
            return new InnerLoader<>(getContext(), args, loadInBackgroundListener);
        }
    }

    private static class InnerLoader<TData> extends AsyncTaskLoader<TData> {
        private final OnLoadInBackgroundListener<TData> listener;

        public InnerLoader(Context context, Bundle args, OnLoadInBackgroundListener<TData> listener) {
            super(context, args);
            this.listener = listener;
        }

        @Override
        protected TData doLoadInBackground(Context context, Bundle args) throws Exception {
            return listener.onLoadInBackground(context, args);
        }
    }

    public interface OnLoadInBackgroundListener<TData> {
        TData onLoadInBackground(Context context, Bundle args) throws Exception;
    }

    public static abstract class Callbacks<TData> {
        private final LoaderCallbacks.Listener<TData> callbacksListener =
                new LoaderCallbacks.Listener<TData>() {
                    @Override
                    public void onLoadFinished(final TData data) {
                        Callbacks.this.onLoadFinished(data);
                    }

                    @Override
                    public void onError(final Exception e) {
                        Callbacks.this.onError(e);
                    }
                };

        private final OnLoadInBackgroundListener<TData> loadInBackgroundListener =
                new OnLoadInBackgroundListener<TData>() {
                    @Override
                    public TData onLoadInBackground(Context context, Bundle args) throws Exception {
                        return Callbacks.this.onLoadInBackground(context, args);
                    }
                };

        public abstract void onLoadFinished(TData data);

        public abstract void onError(Exception e);

        public abstract TData onLoadInBackground(Context context, Bundle args) throws Exception;
    }

    private final LoaderCallbacks.Listener<TOutputData> innerCallbacksListener = new LoaderCallbacks.Listener<TOutputData>() {
        @Override
        public void onLoadFinished(TOutputData tOutputData) {
            DataLoader.this.onLoadFinished(tOutputData);
        }

        @Override
        public void onError(Exception exception) {
            DataLoader.this.onError(exception);
        }
    };

    private final OnLoadInBackgroundListener<TOutputData> innerLoadInBackgroundListener = new OnLoadInBackgroundListener<TOutputData>() {
        @Override
        public TOutputData onLoadInBackground(Context context, Bundle args) throws Exception {
            return DataLoader.this.onLoadInBackground(context, args);
        }
    };

    private final LoaderCallbacks.Listener<TOutputData> callbacksListener;
    private final OnLoadInBackgroundListener<TOutputData> loadInBackgroundListener;

    public DataLoader() {
        this(null, null);
    }

    @Deprecated
    public DataLoader(@NonNull Callbacks<TOutputData> callbacks) {
        this(callbacks.callbacksListener, callbacks.loadInBackgroundListener);
    }

    @Deprecated
    public DataLoader(LoaderCallbacks.Listener<TOutputData> callbacksListener,
                      OnLoadInBackgroundListener<TOutputData> loadInBackgroundListener) {
        this.callbacksListener = callbacksListener;
        this.loadInBackgroundListener = loadInBackgroundListener;
    }

    public void startLoading(Fragment fragment) {
        this.startLoading(fragment, null);
    }

    public void startLoading(Fragment fragment, Bundle args) {
        this.startLoading(fragment.getContext(), fragment.getLoaderManager(), args);
    }

    public void startLoading(AppCompatActivity activity) {
        this.startLoading(activity, (Bundle) null);
    }

    public void startLoading(AppCompatActivity activity, Bundle args) {
        this.startLoading(activity, activity.getSupportLoaderManager(), args);
    }

    public void startLoading(Context context, LoaderManager loaderManager) {
        this.startLoading(context, loaderManager, null);
    }

    public void startLoading(Context context, LoaderManager loaderManager, Bundle args) {
        final InnerCallbacks<TOutputData> innerCallbacks = new InnerCallbacks<>(
                context,
                innerCallbacksListener,
                innerLoadInBackgroundListener
        );

        loaderManager
                .restartLoader(0, args, innerCallbacks)
                .forceLoad();
    }

    public TOutputData onLoadInBackground(Context context, Bundle args) throws Exception {
        if (loadInBackgroundListener != null) {
            return loadInBackgroundListener.onLoadInBackground(context, args);
        }

        return null;
    }

    public void onLoadFinished(TOutputData tOutputData) {
        if (callbacksListener != null) {
            callbacksListener.onLoadFinished(tOutputData);
        }
    }

    public void onError(Exception exception) {
        if (callbacksListener != null) {
            callbacksListener.onError(exception);
        }
    }
}
