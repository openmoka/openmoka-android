/*
 * Copyright (C) 2013 The OpenMoka Project by Mokaware
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.openmoka.android.util;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Utility class to execute useful Service-related tasks
 */
public class ServiceUtils {

    /**
     * Checks whether the service corresponding to the given serviceClass is running.
     * @param context Any context that has already been creates..
     * @param serviceClass Class corresponding to the Service to be searched for.
     * @return <b>true</b> if the service is already running, <b>false</b> otherwise.
     */
    public static boolean isServiceRunning(@NonNull Context context, @NonNull Class<? extends Service> serviceClass) {
        final ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final String serviceClassName = serviceClass.getName().intern();
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClassName.equals(service.service.getClassName())) {
                return true;
            }
        }

        return false;
    }
}
