/*
 * Copyright (C) 2013 The OpenMoka Project by Mokaware
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.openmoka.android.util;

/**
 * This class provides some handy methods to handle common text needs
 */
public class StringUtils {
    /**
     * Representation of an empty {@link String}
     */
    public static final String EMPTY = "";

    /**
     * Validates whether the provided {@link CharSequence} is either null or empty
     * @param text The text to be validated
     * @return <b>true</b> if the provided text is either null or empty.
     */
    public static boolean isEmpty(CharSequence text) {
        return text == null || text.length() == 0;
    }

    /**
     * Checks if the provided text is either null or empty and returns the default value in either
     * case, or the provided text otherwise.
     * @param text The text to be validated
     * @param defaultValue Value to be returned if the provided text is either null or empty
     * @return the provided text if it's not null nor empty, or the default value otherwise.
     */
    public static CharSequence ifEmpty(CharSequence text, CharSequence defaultValue) {
        return isEmpty(text) ? defaultValue : text;
    }

    /**
     * Validates whether the provided {@link CharSequence} is either null or empty
     * @param text The text to be validated
     * @return <b>true</b> if the provided text is either null or empty.
     */
    public static boolean isEmpty(String text) {
        return isEmpty((CharSequence) text);
    }

    /**
     * Checks if the provided text is either null or empty and returns the default value in either
     * case, or the provided text otherwise.
     * @param text The text to be validated
     * @param defaultValue Value to be returned if the provided text is either null or empty
     * @return the provided text if it's not null nor empty, or the default value otherwise.
     */
    public static String ifEmpty(String text, String defaultValue) {
        return isEmpty(text) ? defaultValue : text;
    }
}
