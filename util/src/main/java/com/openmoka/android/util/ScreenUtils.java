/*
 * Copyright (C) 2013 The OpenMoka Project by Mokaware
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.openmoka.android.util;

import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import java.lang.reflect.Method;


/**
 * Utility class to execute useful Screen-related tasks
 */
public class ScreenUtils {
    private static final String TAG = ScreenUtils.class.getSimpleName();

    /**
     * Obtains the screen size, using the given context to retrieve the data.
     * @param context Any context which has already been created.
     * @return The screen size wrapped in a {@link Point}
     */
    public static Point getScreenSize(Context context) {
        final WindowManager windowManager =
                (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

        return getScreenSize(windowManager);
    }

    /**
     * Obtains the screen size, using the given window manager to retrieve the data.
     * @param windowManager the {@link WindowManager} to be used to retrieve the screen data.
     * @return The screen size wrapped in a {@link Point}
     */
    public static Point getScreenSize(WindowManager windowManager) {
        final Display display = windowManager.getDefaultDisplay();
        final int screenWidth;
        final int screenHeight;
        if (Build.VERSION.SDK_INT >= 17) {
            //new pleasant way to get real metrics
            Point size = new Point();
            display.getRealSize(size);
            screenWidth = size.x;
            screenHeight = size.y;
        } else if (Build.VERSION.SDK_INT >= 14) {
            //reflection for this weird in-between time
            int w;
            int h;
            try {
                Method mGetRawH = Display.class.getMethod("getRawHeight");
                Method mGetRawW = Display.class.getMethod("getRawWidth");
                w = (Integer) mGetRawW.invoke(display);
                h = (Integer) mGetRawH.invoke(display);
            } catch (Exception e) {
                //this may not be 100% accurate, but it's all we've got
                w = display.getWidth();
                h = display.getHeight();
                Log.e(TAG, "Couldn't use reflection to get the real display metrics.");
            }

            screenWidth = w;
            screenHeight = h;

        } else {
            //This should be close, as lower API devices should not have window navigation bars
            screenWidth = display.getWidth();
            screenHeight = display.getHeight();
        }

        return new Point(screenWidth, screenHeight);
    }
}
