/*
 * Copyright (C) 2013 The OpenMoka Project by Mokaware
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.openmoka.android.util;

/**
 * Utility class which provides handy methods for any object
 */
public class ObjectUtils {

    /**
     *  Returns the provided value if it's not null, or the default value otherwise.
     * @param value the object to be tested
     * @param defaultValue object to be returned if the provided value is null.
     * @param <T> The type of the provided value and default value objects.
     * @return The provided value if it's not null, or the default value otherwise.
     */
    public static <T> T ifNull(T value, T defaultValue) {
        return value != null ? value : defaultValue;
    }
}
