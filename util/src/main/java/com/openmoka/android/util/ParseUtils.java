/*
 * Copyright (C) 2013 The OpenMoka Project by Mokaware
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.openmoka.android.util;

/**
 * Utility class to ease parsing Strings
 */
public class ParseUtils {

    /**
     * <p>
     * Tries to parse a String to a boolean representation.
     * </p>
     * <p>
     * Two scenarios are supported for the provided text:
     * <ul>
     *     <li>
     *         Integer representation: In this case the <b>zero</b> is considered <b>false</b>,
     *         and any other value is <b>true</b>
     *     </li>
     *     <li>
     *         Text representation: The evaluation is the same as {@link Boolean#parseBoolean(String)}
     *     </li>
     * </ul>
     * </p>
     *
     * @param s The String to try to parse
     * @return <b>true</b> when the provided String is either an int with value different
     * than zero, <b>false</b> if the value equals zero, or the result as evaluated
     * by {@link Boolean#parseBoolean(String)}.
     */
    public static boolean tryParseBoolean(String s) {
        try {
            final int tryInt = Integer.parseInt(s);
            return tryInt != 0;
        } catch (Exception e) {
            // String wasn't an integer representation
        }

        return Boolean.parseBoolean(s);
    }

    /**
     * Tries to parse a String as byte.
     * @param s The String to be parsed.
     * @param defaultValue Value to be returned when the parsing fails.
     * @return The value that was correctly parsed, or the default value if the parsing fails.
     */
    public static byte tryParseByte(String s, byte defaultValue) {
        try {
            return Byte.parseByte(s);
        } catch (Exception e) {
            // Couldn't parse the String, return the default value
        }

        return defaultValue;
    }

    /**
     * Tries to parse a String as short.
     * @param s The String to be parsed.
     * @param defaultValue Value to be returned when the parsing fails.
     * @return The value that was correctly parsed, or the default value if the parsing fails.
     */
    public static short tryParseShort(String s, short defaultValue) {
        try {
            return Short.parseShort(s);
        } catch (Exception e) {
            // Couldn't parse the String, return the default value
        }

        return defaultValue;
    }

    /**
     * Tries to parse a String as int.
     * @param s The String to be parsed.
     * @param defaultValue Value to be returned when the parsing fails.
     * @return The value that was correctly parsed, or the default value if the parsing fails.
     */
    public static int tryParseInt(String s, int defaultValue) {
        try {
            return Integer.parseInt(s);
        } catch (Exception e) {
            // Couldn't parse the String, return the default value
        }

        return defaultValue;
    }

    /**
     * Tries to parse a String as long.
     * @param s The String to be parsed.
     * @param defaultValue Value to be returned when the parsing fails.
     * @return The value that was correctly parsed, or the default value if the parsing fails.
     */
    public static long tryParseLong(String s, long defaultValue ) {
        try {
            return Long.parseLong(s);
        } catch (Exception e) {
            // Couldn't parse the String, return the default value
        }

        return defaultValue;
    }

    /**
     * Tries to parse a String as float.
     * @param s The String to be parsed.
     * @param defaultValue Value to be returned when the parsing fails.
     * @return The value that was correctly parsed, or the default value if the parsing fails.
     */
    public static float tryParseFloat(String s, float defaultValue) {
        try {
            return Float.parseFloat(s);
        } catch (Exception e) {
            // Couldn't parse the String, return the default value
        }

        return defaultValue;
    }

    /**
     * Tries to parse a String as double.
     * @param s The String to be parsed.
     * @param defaultValue Value to be returned when the parsing fails.
     * @return The value that was correctly parsed, or the default value if the parsing fails.
     */
    public static double tryParseDouble(String s, double defaultValue) {
        try {
            return Double.parseDouble(s);
        } catch (Exception e) {
            // Couldn't parse the String, return the default value
        }

        return defaultValue;
    }
}
